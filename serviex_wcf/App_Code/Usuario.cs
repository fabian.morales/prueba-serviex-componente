﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Descripción breve de User
/// </summary>
[DataContract]
public class Usuario
{
	[DataMember]
	public int id { get; set; }
	[DataMember]
	public string nombre { get; set; }

	[DataMember]
	public string sexo { get; set; }

	[DataMember]
	public DateTime fechaNacimiento { get; set; }
	public Usuario()
	{
	}
}