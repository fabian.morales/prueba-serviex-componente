﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;

// NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
public class Service : IService
{
	public Usuario[] obtenerListaUsuarios()
	{
		string sql = "select * from usuario";
		List<Usuario> usuarios = new List<Usuario>();
		BaseDatos bd = new BaseDatos();
		bd.connect();
		DataTable dtRes = bd.openDataTable(sql);
		if (dtRes.Rows.Count > 0)
		{
			var query = from a in dtRes.Rows.Cast<DataRow>()
						select new Usuario
						{
							id = int.Parse(a["id"].ToString()),
							nombre = a["nombre"].ToString(),
							sexo = a["sexo"].ToString(),
							fechaNacimiento = DateTime.Parse(a["fecha_nacimiento"].ToString())
						};

			usuarios = query.ToList();
		}
		bd.close();
		return usuarios.ToArray();
	}

	public Usuario obtenerUsuario(int id)
	{
		string sql = "select * from usuario where id = ? limit 0, 1";
		List<MySql.Data.MySqlClient.MySqlParameter> parametros = new List<MySql.Data.MySqlClient.MySqlParameter>();
		parametros.Add(new MySql.Data.MySqlClient.MySqlParameter("id", id));

		Usuario usuario = null;
		BaseDatos bd = new BaseDatos();
		bd.connect();
		DataTable dtRes = bd.openDataTable(sql, parametros);
		if (dtRes.Rows.Count > 0)
		{
			var query = from a in dtRes.Rows.Cast<DataRow>()
						select new Usuario
						{
							id = int.Parse(a["id"].ToString()),
							nombre = a["nombre"].ToString(),
							sexo = a["sexo"].ToString(),
							fechaNacimiento = DateTime.Parse(a["fecha_nacimiento"].ToString())
						};

			usuario = query.First();
		}
		bd.close();
		return usuario;
	}

	public int crearUsuario(string nombre, DateTime fechaNacimiento, string sexo)
	{
		string sql = "insert into usuario (nombre, fecha_nacimiento, sexo) values (?, ?, ?)";

		List<MySql.Data.MySqlClient.MySqlParameter> parametros = new List<MySql.Data.MySqlClient.MySqlParameter>();
		parametros.Add(new MySql.Data.MySqlClient.MySqlParameter("nombre", nombre));
		parametros.Add(new MySql.Data.MySqlClient.MySqlParameter("fechaNacimiento", fechaNacimiento));
		parametros.Add(new MySql.Data.MySqlClient.MySqlParameter("sexo", sexo));

		BaseDatos bd = new BaseDatos();
		bd.connect();
		int ret = bd.execute(sql, parametros);
		bd.close();

		return ret;
	}

	public int actualizarUsuario(int id, string nombre, DateTime fechaNacimiento, string sexo)
	{
		string sql = "update usuario set nombre = ?, fecha_nacimiento = ?, sexo = ? where id = ?";

		List<MySql.Data.MySqlClient.MySqlParameter> parametros = new List<MySql.Data.MySqlClient.MySqlParameter>();
		parametros.Add(new MySql.Data.MySqlClient.MySqlParameter("nombre", nombre));
		parametros.Add(new MySql.Data.MySqlClient.MySqlParameter("fechaNacimiento", fechaNacimiento));
		parametros.Add(new MySql.Data.MySqlClient.MySqlParameter("sexo", sexo));
		parametros.Add(new MySql.Data.MySqlClient.MySqlParameter("id", id));

		BaseDatos bd = new BaseDatos();
		bd.connect();
		int ret = bd.execute(sql, parametros);
		bd.close();

		return ret;
	}

	public int borrarUsuario(int id)
	{
		string sql = "delete from usuario where id = ?";

		List<MySql.Data.MySqlClient.MySqlParameter> parametros = new List<MySql.Data.MySqlClient.MySqlParameter>();
		parametros.Add(new MySql.Data.MySqlClient.MySqlParameter("id", id));

		BaseDatos bd = new BaseDatos();
		bd.connect();
		int ret = bd.execute(sql, parametros);
		bd.close();

		return ret;
	}

}
