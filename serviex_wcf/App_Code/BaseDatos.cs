﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;

/// <summary>
/// Descripción breve de BaseDatos
/// </summary>
public class BaseDatos
{
	public BaseDatos()
	{
        
    }

    string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MysqlProd"].ConnectionString;
    MySqlConnection connection;

    public bool connect()
    {
        try
        {
            if (connection == null)
            {
                connection = new MySqlConnection(connectionString);
            }

            if (connection.State == ConnectionState.Closed || connection.State == ConnectionState.Broken)
            {
                connection.Open();
            }

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public bool close()
    {
        try
        {
            if (connection != null && connection.State != ConnectionState.Closed)
            {
                connection.Close();
            }

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public DataTable openDataTable(string sql, List<MySqlParameter> parametros = null)
    {
        MySqlCommand command = new MySqlCommand(sql, connection);
        if (parametros != null)
        {
            command.Parameters.AddRange(parametros.ToArray());
        }

        MySqlDataAdapter adapter = new MySqlDataAdapter(command);
        DataTable ret = new DataTable();
        adapter.Fill(ret);
        return ret;
    }

    public int execute(string sql, List<MySqlParameter> parametros = null)
    {
        int ret = 0;
        MySqlCommand command = new MySqlCommand(sql, connection);
        if (parametros != null)
		{
            command.Parameters.AddRange(parametros.ToArray());
        }
        
        ret = command.ExecuteNonQuery();
        return ret;
    }
}