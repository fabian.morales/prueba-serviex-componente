﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
[ServiceContract]
public interface IService
{

	[OperationContract]
	Usuario[] obtenerListaUsuarios();

	[OperationContract]
	Usuario obtenerUsuario(int id);

	[OperationContract]
	int crearUsuario(string nombre, DateTime fechaNacimiento, string sexo);

	[OperationContract]
	int actualizarUsuario(int id, string nombre, DateTime fechaNacimiento, string sexo);

	[OperationContract]
	int borrarUsuario(int id);
}

