﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using serviex_Web.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace serviex_Web.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;

		public HomeController(ILogger<HomeController> logger)
		{			
			_logger = logger;
		}

		public IActionResult Index()
		{
			Models.EditarViewModel modelo = new EditarViewModel();
			modelo.usuario = new ServiceReference1.Usuario();
			return View("Index", modelo);
		}

		public IActionResult Lista()
		{
			ServiceReference1.ServiceClient wcf = new ServiceReference1.ServiceClient();
			Task<ServiceReference1.Usuario[]> tarea = wcf.obtenerListaUsuariosAsync();
			tarea.Wait();
			Models.ListaViewModel modelo = new ListaViewModel();
			modelo.usuarios = tarea.Result;
			return View(modelo);
		}

		public IActionResult Editar(int id)
		{
			ServiceReference1.ServiceClient wcf = new ServiceReference1.ServiceClient();
			Task<ServiceReference1.Usuario> tarea = wcf.obtenerUsuarioAsync(id);
			Models.EditarViewModel modelo = new EditarViewModel();
			tarea.Wait();
			modelo.usuario = tarea.Result;
			return View(modelo);
		}


		[HttpPost]
		public IActionResult Guardar(ServiceReference1.Usuario usuario)
		{
			ServiceReference1.ServiceClient wcf = new ServiceReference1.ServiceClient();
			Task<int> tarea = wcf.crearUsuarioAsync(usuario.nombre, usuario.fechaNacimiento, usuario.sexo);
			tarea.Wait();
			if (tarea.Result == 1)
			{
				TempData["mensaje_exito"] = "Usuario creado exitosamente";
			}
			else
			{
				TempData["mensaje_error"] = "No se pudo crear el usuario";
			}
			
			return RedirectToAction("Index");
		}

		public IActionResult Borrar(int id)
		{
			ServiceReference1.ServiceClient wcf = new ServiceReference1.ServiceClient();
			Task<int> tarea = wcf.borrarUsuarioAsync(id);
			Models.EditarViewModel modelo = new EditarViewModel();
			tarea.Wait();
			if (tarea.Result == 1)
			{
				TempData["mensaje_exito"] = "Usuario borrado exitosamente";
			}
			else
			{
				TempData["mensaje_error"] = "No se pudo borrar el usuario";
			}
			return RedirectToAction("Index");
		}

		[HttpPost]
		public IActionResult Actualizar(ServiceReference1.Usuario usuario)
		{
			ServiceReference1.ServiceClient wcf = new ServiceReference1.ServiceClient();
			Task<int> tarea = wcf.actualizarUsuarioAsync(usuario.id, usuario.nombre, usuario.fechaNacimiento, usuario.sexo);
			tarea.Wait();
			if (tarea.Result == 1)
			{
				TempData["mensaje_exito"] = "Usuario actualizado exitosamente";
			}
			else
			{
				TempData["mensaje_error"] = "No se pudo actualizar el usuario";
			}
			return RedirectToAction("Index");
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
