﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace serviex_Web.Models
{
	public class EditarViewModel
	{
		public ServiceReference1.Usuario usuario;
		public Dictionary<string, string> sexos = new Dictionary<string, string>()
		{
			{ "M", "Masculino" },
			{ "F", "Femenino" }
		};
	}
}
